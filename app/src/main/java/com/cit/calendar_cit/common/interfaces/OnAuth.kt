package com.cit.calendar_cit.common.interfaces

import com.cit.calendar_cit.common.models.ModelUser

interface OnAuth {
    fun onGood(modelUser: ModelUser)
}
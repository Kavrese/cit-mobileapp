package com.cit.calendar_cit.common

import com.cit.calendar_cit.common.models.*

/*
    Класс с заранее подготовленными тестовыми данными.
*/
class StaticData {
    companion object{
        val listAvatarsBoss = arrayListOf(
            ModelAvatar(0, "Test", "Test", "Test", "https://i.pinimg.com/236x/bf/6c/36/bf6c365f1078cbbb157226f67dcf4881.jpg"),
            ModelAvatar(1, "Test", "Test", "Test", "https://www.meme-arsenal.com/memes/6381871fb8afa41cab763e7309b86bfb.jpg"),
        )
        val listCategoryAvatars = arrayListOf(
            ModelCategoryUsersAvatars("Руководители", listAvatarsBoss),
            ModelCategoryUsersAvatars("Будут присутствовать", arrayListOf(
                    ModelAvatar(2, "Test", "Test", "Test", "https://i.pinimg.com/236x/e0/95/ba/e095ba516be342b0dba2dd8fee99da3a.jpg"),
                    ModelAvatar(3, "Test", "Test", "Test", "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/4d/4d20fa16b63e3c3ad889a06c936825b77e049970_full.jpg"),
                    ModelAvatar(4, "Test", "Test", "Test", "https://i.pinimg.com/474x/e3/31/57/e33157ea21bd33ddea822beb78f6df16.jpg"),
                )),
            ModelCategoryUsersAvatars("Должны присутствовать", arrayListOf(
                ModelAvatar(5, "Test", "Test", "Test", "https://steamavatar.io/img/14773519040Sv21.jpg"),
            )),
            ModelCategoryUsersAvatars("Освобожденны", arrayListOf())
        )
        val modelUser = ModelUser(0, "Firstname", "Secondname", "Lastname", "https://forum.truckersmp.com/uploads/monthly_2019_06/imported-photo-186659.thumb.jpeg.7ca80c40fa6e972e04cc2f14f5114d80.jpeg",0, "token")
        val listSevenDays = arrayListOf(
            Codes.modelDayFromStrDate("7.01.22"),
            Codes.modelDayFromStrDate("8.01.22"),
            Codes.modelDayFromStrDate("9.01.22"),
            Codes.modelDayFromStrDate("10.01.22"),
            Codes.modelDayFromStrDate("11.01.22"),
            Codes.modelDayFromStrDate("12.01.22"),
            Codes.modelDayFromStrDate("13.01.22"),
        )
        val listDataDays = arrayListOf<ModelDayData>(
            ModelDayData(listSevenDays[0], arrayListOf(
                ModelEvent(
                    "Test1", 1, listSevenDays[0].date, "13:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
                ModelEvent(
                    "Test2", 1, listSevenDays[0].date, "14:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
                ModelEvent(
                    "Test3", 1, listSevenDays[0].date, "15:10",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
                ModelEvent(
                    "Test4", 1, listSevenDays[0].date, "13:20",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
                ModelEvent(
                    "Test5", 1, listSevenDays[0].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
            )),
            ModelDayData(listSevenDays[1], arrayListOf(
                ModelEvent(
                    "Test1", 1, listSevenDays[1].date, "13:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
                ModelEvent(
                    "Test2", 1, listSevenDays[1].date, "13:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                )
            )),
            ModelDayData(listSevenDays[2], arrayListOf(
                ModelEvent(
                    "Test1", 1, listSevenDays[2].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ),
            )),
            ModelDayData(listSevenDays[3], arrayListOf()),
            ModelDayData(listSevenDays[4], arrayListOf()),
            ModelDayData(listSevenDays[5], arrayListOf()),
            ModelDayData(listSevenDays[6], arrayListOf()),
        )

        val notifications : List<ModelNotification> = arrayListOf(
            ModelNotification(
                ModelEvent(
                    "Test1", 1, listSevenDays[2].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ), "Test Notification Event 1", "10:00", "01.12.21", false, null),
            ModelNotification(
                ModelEvent(
                    "Test2", 1, listSevenDays[2].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ), "Test Notification Event 2", "11:00", "01.12.21", false, null),
            ModelNotification(
                ModelEvent(
                    "Test3", 1, listSevenDays[2].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ), "Test Notification Event 3", "13:00", "02.12.21", false, null),
            ModelNotification(
                ModelEvent(
                    "Test1", 1, listSevenDays[2].date, "01:00",
                    arrayListOf(),  arrayListOf(),  arrayListOf(),  arrayListOf(),
                ), "Test Notification Event 1", "12:00", "31.11.21", false, null),
        )
    }
}
package com.cit.calendar_cit.common

import android.content.Context
import android.view.View
import com.cit.calendar_cit.Dialogs.ChooseDialog
import com.cit.calendar_cit.Dialogs.ListDialog
import com.cit.calendar_cit.Dialogs.MessageDialog
import com.cit.calendar_cit.Dialogs.PlatformDialog

class FactoryDialog {
    companion object {
        var throwable: Throwable? = null
        fun getDialog(context: Context, messageCode: MessageCodeDialog?=null, canDismissAfterClick: Boolean=true,
                      typeDialog: TypeDialog, messageCodes: List<MessageCodeDialog>?=null,
                      onClickElementList: View.OnClickListener?=null,
                      onClick1: View.OnClickListener?=null,
                      onClick2: View.OnClickListener?=null, mapButtons: Map<String, View.OnClickListener>?=null, dataList: List<Any>?=null,
        throwable: Throwable?=null): PlatformDialog {
            this.throwable = throwable
            return when(typeDialog){
                TypeDialog.DIALOG_LIST_USERS -> buildListDialog(context, dataList!!, messageCode!!, onClickElementList, typeDialog)
                TypeDialog.DIALOG_LIST_TEMPLATES -> buildListDialog(context, dataList!!, messageCode!!, onClickElementList, typeDialog)
                TypeDialog.DELETE_DIALOG -> buildMessageDialog(context, messageCode!!, onClick1, onClick2, canDismissAfterClick=canDismissAfterClick)
                TypeDialog.MESSAGE_DIALOG -> buildMessageDialog(context, messageCode!!, onClick1, onClick2, canDismissAfterClick=canDismissAfterClick)
                TypeDialog.MESSAGES_DIALOG -> buildMessagesDialog(context, messageCodes!!, onClick1, onClick2, canDismissAfterClick=canDismissAfterClick)
                TypeDialog.CHOOSE_DIALOG -> buildChooseDialog(context, mapButtons!!)
            }
        }

        private fun buildMessagesDialog(context: Context, codesAlertDialogMessage: List<MessageCodeDialog>,
                                        onClickYes: View.OnClickListener?=null,
                                        onClickNo: View.OnClickListener?=null,
                                        textButton1: String?=null, textButton2: String?=null,
                                        canDismissAfterClick: Boolean=true): PlatformDialog{
            return MessageDialog.Builder(context)
                .setTitle(getTitleForManyCodes())
                .setMessage(parseAllMessageToOne(codesAlertDialogMessage))
                .setOnClickButton1(onClickYes)
                .setOnClickButton2(onClickNo)
                .setDismissAfterClickButton(canDismissAfterClick)
                .setTextButton1(textButton1)
                .setTextButton2(textButton2)
                .create()
        }

        private fun buildMessageDialog(context: Context, messageCode: MessageCodeDialog,
                                       onClick1: View.OnClickListener?=null,
                                       onClick2: View.OnClickListener?=null,
                                       textButton1: String?=null, textButton2: String?=null,
                                       canDismissAfterClick: Boolean=true
        ): PlatformDialog {
            return MessageDialog.Builder(context)
                .setTitle(getTitle(messageCode))
                .setMessage(getMessage(messageCode, throwable))
                .setOnClickButton1(onClick1)
                .setOnClickButton2(onClick2)
                .setTextButton1(textButton1)
                .setTextButton2(textButton2)
                .setDismissAfterClickButton(canDismissAfterClick)
                .create()
        }

        private fun buildChooseDialog(context: Context,
                                      data: Map<String, View.OnClickListener>,
                                      canDismissAfterClick: Boolean=true): PlatformDialog {
            return ChooseDialog.Builder(context)
                .setDataForAdapter(data)
                .setDismissAfterClick(canDismissAfterClick)
                .create()
        }

        private fun buildListDialog(context: Context, data: List<Any>, messageCode: MessageCodeDialog,
                                    onClickElement: View.OnClickListener?, typeDialog: TypeDialog,
                                    canDismissAfterClick: Boolean=true): PlatformDialog {
            return ListDialog.Builder(context)
                .setDataForAdapter(data, typeDialog)
                .setTitle(getTitle(messageCode))
                .setDismissAfterClickButton(canDismissAfterClick)
                .setOnClickElement(onClickElement)
                .create()
        }

        private fun getMessage(messageCode: MessageCodeDialog, throwable: Throwable?=null): String{
            return if (throwable == null) getMessageFromCode(messageCode) else throwable.message!!
        }

        private fun parseAllMessageToOne(messageCodes: List<MessageCodeDialog>): String{
            return messageCodes.joinToString(separator = "\n", transform={ getMessageFromCode(it)})
        }

        private fun getTitle(messageCode: MessageCodeDialog): String{
            return getTitleCode(messageCode)
        }

        private fun getTitleForManyCodes(): String{
            return "Множественные ошибки"
        }

        private fun getTitleCode(messageCode: MessageCodeDialog): String{
            return messageCode.title
        }

        private fun getMessageFromCode(messageCode: MessageCodeDialog): String{
            return messageCode.message
        }
    }
}

enum class MessageCodeDialog(val title: String, val message: String){
    AUTH_FAIL("Ошибка Авторизации", "Повторите попытку позже !"),
    AUTH_NULL("Ошибка Авторизации", "Повторите попытку позже !"),
    EMPTY_FIELDS("Ошибка", "Заполните все поля !"),
    TOKEN_NOT_AVAILABLE("Ошибка подлючения", "Токен не действителен"),
    TEST("Это тест", "Привет )"),
    PASSWORD_NOT_CORRECT("Пароль не корректен. Минимум 6 символов", "Исправте и повторите попытку !"),
    EMAIL_NOT_CORRECT("Почта не корректна", "Исправте и повторите попытку !"),
    EMPTY_FIELD("Ошибка заполнение поля", "Одно обязательно поле не заполненно. Заполните все поля"),

    LIST_DIALOG_USERS("Выберите пользователя", ""),
    LIST_DIALOG_TEMPLATES("Выберите шаблон", "")
}

enum class TypeDialog{
    DELETE_DIALOG, MESSAGE_DIALOG, MESSAGES_DIALOG, CHOOSE_DIALOG, DIALOG_LIST_USERS, DIALOG_LIST_TEMPLATES
}
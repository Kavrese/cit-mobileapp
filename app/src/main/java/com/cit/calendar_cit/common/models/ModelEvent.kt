package com.cit.calendar_cit.common.models

data class ModelEvent (
    val title: String,
    val durationHour: Int,
    val date: String,
    val time: String,
    val usersBossIds: List<Int>,
    val usersExistIds: List<Int>,
    val usersCanExistIds: List<Int>,
    val usersFreeIds: List<Int>,
    val href: String? = "https://drive.google.com/drive/u/0/folders/1EGIeYHrpaoGfg-pi0l7p45eNCjJBetUp"
)

package com.cit.calendar_cit.common.models

data class BodyLogin (
    val login: String,
    val password: String
)
package com.cit.calendar_cit.common.models

data class ModelNotification (
    val event: ModelEvent?,
    val title: String,
    val time: String,
    val date: String,
    val custom: Boolean,
    val text: String?
)

package com.cit.calendar_cit.common.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cit.calendar_cit.Fragments.FragmentCalendarDay
import com.cit.calendar_cit.common.models.ModelDay

class AdapterFragmentCalendarDay(fa: FragmentActivity, val days: List<ModelDay>): FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = days.size

    override fun createFragment(position: Int): Fragment {
        return FragmentCalendarDay(days[position])
    }
}
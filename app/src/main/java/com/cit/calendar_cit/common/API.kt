package com.cit.calendar_cit.common

import com.cit.calendar_cit.common.models.BodyLogin
import com.cit.calendar_cit.common.models.ModelUser
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface API {
    @POST("login")
    fun auth(@Body body: BodyLogin): Call<ModelUser>
}
package com.cit.calendar_cit.common

import com.cit.calendar_cit.common.models.ModelAvatar
import com.cit.calendar_cit.common.models.ModelEvent

object ToNextScreen {
    lateinit var event: ModelEvent
    lateinit var avatar: ModelAvatar
}
package com.cit.calendar_cit.common.interfaces

import android.view.View
import com.cit.calendar_cit.common.models.ModelEvent

interface OnClickEvent{
    fun onClick(event: ModelEvent)
}
package com.cit.calendar_cit.common.models

class ModelUser(
    val userId: Int,
    val firstname: String,
    val secondname: String,
    val lastname: String,
    val avatarURL: String?,
    type_id: Int,
    val token: String?
    ){
    // Я не знаю будет ли это работать при получении из Retrofit
    val type: TypeUser = parseIdTypeToTypeUser(type_id)
}

fun parseIdTypeToTypeUser(type_id: Int): TypeUser = when(type_id) {
    0 -> TypeUser.TYPE_DEFAULT
    1 -> TypeUser.TYPE_ASSISTANT
    2 -> TypeUser.TYPE_BOSS
    3 -> TypeUser.TYPE_ADMIN
    else -> TypeUser.TYPE_UNKNOWN
}

enum class TypeUser(val id: Int, val title: String, val weight: Int){
    TYPE_UNKNOWN(-1, "Неизвестно", -1),
    TYPE_DEFAULT(0, "Ученик", 0),
    TYPE_ASSISTANT(1, "Ассистент", 1),
    TYPE_BOSS(2, "Руководитель", 2),
    TYPE_ADMIN(3, "Админ", 3)
}

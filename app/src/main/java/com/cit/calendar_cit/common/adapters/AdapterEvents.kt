package com.cit.calendar_cit.common.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.interfaces.OnClickEvent
import com.cit.calendar_cit.common.models.ModelEvent
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.item_time_event.view.*

class AdapterTimeEvents(events: List<ModelEvent>, private val onClickEvent: OnClickEvent): RecyclerView.Adapter<AdapterTimeEvents.ViewHolder>() {
    lateinit var listTimeEvents: List<ModelTimeEvents>
    init {
        newListEvents(events)
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    class AdapterEvents(private var events: List<ModelEvent>, private val onClickEvent: OnClickEvent): RecyclerView.Adapter<ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false))
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.card_event.setOnClickListener {
                onClickEvent.onClick(events[position])
            }
            holder.itemView.title_event.text = events[position].title
            holder.itemView.duration_event.text = "${events[position].durationHour} ${
                Codes.getStrAfterHour(
                    events[position].durationHour
                )
            }"
            holder.itemView.rec_avatars.apply {
                layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = AdapterAvatars(StaticData.listAvatarsBoss, TypeAvatar.SMALL)
            }
        }

        override fun getItemCount(): Int = events.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_time_event, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.up_part.setImageResource(if (position == 0) R.drawable.up else R.drawable.dot)
        holder.itemView.down_part.visibility = if (position == listTimeEvents.lastIndex) View.VISIBLE else View.GONE
        holder.itemView.time_events.text = listTimeEvents[position].time
        holder.itemView.rec_events.apply {
            adapter = AdapterEvents(listTimeEvents[position].events, onClickEvent)
            layoutManager = LinearLayoutManager(holder.itemView.context)
        }
    }

    override fun getItemCount(): Int = listTimeEvents.size

    fun newListEvents(new_events: List<ModelEvent>){
        val listTime = new_events.map{it.time}.toSet().toList()
        val newListTimeEvents = listTime.map { time -> ModelTimeEvents(time, new_events.filter{it.time == time}) }
        listTimeEvents = newListTimeEvents
        notifyDataSetChanged()
    }
}

data class ModelTimeEvents(
    val time: String,
    val events: List<ModelEvent>
)
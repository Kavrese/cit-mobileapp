package com.cit.calendar_cit.common.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cit.calendar_cit.ProfileScreen.ProfileActivity
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.ToNextScreen
import com.cit.calendar_cit.common.models.ModelAvatar
import kotlinx.android.synthetic.main.item_avatar_big.view.*

class AdapterAvatars(private val avatars: List<ModelAvatar>,
                     private val typeAvatar: TypeAvatar,
                     private val existButtonAdd: Boolean=false,
                     private val onClickButtonAdd: View.OnClickListener?=null
): RecyclerView.Adapter<AdapterAvatars.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(if (viewType == TypeAvatar.BIG.item_type) typeAvatar.item_res else TypeAvatar.ADD.item_type, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) != TypeAvatar.ADD.item_type) {
            Glide.with(holder.itemView.context)
                .load(avatars[position].avatar)
                .into(holder.itemView.avatar)

            holder.itemView.avatar.setOnClickListener {
                ToNextScreen.avatar = avatars[position]
                holder.itemView.context.startActivity(
                    Intent(
                        holder.itemView.context,
                        ProfileActivity::class.java
                    )
                )
            }
        }else{
            holder.itemView.setOnClickListener(onClickButtonAdd)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (existButtonAdd && position == avatars.lastIndex + 1) TypeAvatar.ADD.item_type else TypeAvatar.BIG.item_type
    }

    override fun getItemCount(): Int = avatars.size + if (existButtonAdd) 1 else 0
}

enum class TypeAvatar(val item_res: Int, val item_type: Int){
    SMALL(R.layout.item_avatar_small, 0), BIG(R.layout.item_avatar_big, 0), ADD(R.layout.item_avatar_add, 1)
}
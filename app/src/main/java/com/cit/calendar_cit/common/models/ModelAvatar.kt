package com.cit.calendar_cit.common.models

data class ModelAvatar(
    val idUser: Int,
    val lastname: String,
    val firstname: String,
    val secondname: String,
    val avatar: String
)

data class ModelCategoryUsersAvatars(
    val title: String,
    val listAvatars: List<ModelAvatar>
)

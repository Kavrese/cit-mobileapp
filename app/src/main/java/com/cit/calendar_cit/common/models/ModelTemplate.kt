package com.cit.calendar_cit.common.models

class ModelTemplate(
    val title: String,
    val users: List<ModelUser>,
    access_id: Int,
){
    // Я не знаю будет ли это работать при получении из Retrofit
    val access: TypeAccess = parseIdTypeToTypeAccess(access_id)
}

fun parseIdTypeToTypeAccess(type_id: Int): TypeAccess = when(type_id) {
    0 -> TypeAccess.TYPE_PRIVATE
    1 -> TypeAccess.TYPE_PUBLIC
    2 -> TypeAccess.TYPE_GROUP
    else -> TypeAccess.TYPE_UNKNOWN
}

enum class TypeAccess(val id: Int, val title: String){
    TYPE_UNKNOWN(-1, "Неизвестно"),
    TYPE_PRIVATE(0, "Личный"),
    TYPE_PUBLIC(1, "Общий"),
    TYPE_GROUP(2, "Груповой"),
}

package com.cit.calendar_cit.common

import android.content.Context
import com.cit.calendar_cit.Dialogs.PlatformDialog
import com.cit.calendar_cit.common.interfaces.OnAuth
import com.cit.calendar_cit.common.models.BodyLogin
import com.cit.calendar_cit.common.models.ModelDay
import com.cit.calendar_cit.common.models.ModelUser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Math.abs
import java.text.SimpleDateFormat
import java.util.*
import androidx.core.content.ContextCompat.startActivity

import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat


class Codes {
    companion object {
        private fun initRetrofit(): API{
            return Retrofit.Builder()
                .baseUrl("")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(API::class.java)
        }

        fun auth(context: Context, login: String, password: String, onAuth: OnAuth){
            initRetrofit().auth(BodyLogin(login, password)).enqueue(object: Callback<ModelUser>{
                override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                    if (response.isSuccessful && response.body() != null) {
                        val user = response.body()!!
                        onAuth.onGood(user)
                    }else{
                        CreatorDialogs.getMessageDialog(context, MessageCodeDialog.AUTH_NULL)
                    }
                }
                override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                    CreatorDialogs.getMessageDialog(context, MessageCodeDialog.AUTH_FAIL, t=t).show()
                }
            })
        }

        private fun validateEmail(email: String): Boolean{
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        private fun validatePassword(password: String): Boolean {
            return password.length >= 6
        }

        fun getStrAfterHour(hour: Int): String{
            return mapOf(0 to "часов", 1 to "час", 2..4 to "часа", 5..9 to "часов")[hour.toString()[hour.toString().lastIndex].toString().toInt()]!!
        }

        fun convertDateToString(date: Date): String {
            return SimpleDateFormat("dd.MM.yy", Locale.ENGLISH).format(date)
        }

        fun convertStringToDate(date: String): Date {
            return SimpleDateFormat("dd.MM.yy", Locale.ENGLISH).parse(date)!!
        }

        fun openUrlInBrowser(context: Context, url: String){
            val openPage = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(Intent.createChooser(openPage, "Выберите браузер"))
        }

        fun getSevenDays(): List<ModelDay> {
            // Пока нет API берем тестовые 7 дней
            return StaticData.listSevenDays
            val calendar = Calendar.getInstance()
            val listDays = arrayListOf<ModelDay>()
            for (i in 0..7) {
                calendar.add(Calendar.DAY_OF_MONTH, 1)
                listDays.add(modelDayFromDate(calendar.time))
            }
            return listDays
        }

        fun modelDayFromDate(date: Date): ModelDay{
            val calendar = Calendar.getInstance()
            calendar.time = date
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val dayOfWeek = SimpleDateFormat("EE", Locale.getDefault()).format(date)
                .uppercase(Locale.getDefault())
            return ModelDay(convertDateToString(date), day, dayOfWeek)
        }

        fun modelDayFromStrDate(date: String): ModelDay{
            val thisDate = convertStringToDate(date)
            val calendar = Calendar.getInstance()
            calendar.time = thisDate
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val dayOfWeek = SimpleDateFormat("EE", Locale.getDefault()).format(thisDate)
                .uppercase(Locale.getDefault())
            return ModelDay(date, day, dayOfWeek)
        }

        fun countBeetwenDateDateNow(date_str: String): Int {
            val date = SimpleDateFormat("dd.MM.yy", Locale.ENGLISH).parse(date_str)!!
            val calendarDate = Calendar.getInstance()
            calendarDate.time = date
            val calendarNow = Calendar.getInstance()
            calendarNow.set(Calendar.HOUR_OF_DAY, 0)
            calendarNow.set(Calendar.MINUTE, 0)

            val countDayDate = calendarDate.get(Calendar.DAY_OF_MONTH)
            val countMonthDate = calendarDate.get(Calendar.MONTH) + 1
            val countYearDate = calendarDate.get(Calendar.YEAR)
            val sumDaysDate = countDayDate + 30 * countMonthDate + 30 * 12 * countYearDate

            val countDayNow = calendarNow.get(Calendar.DAY_OF_MONTH)
            val countMonthNow = calendarNow.get(Calendar.MONTH) + 1
            val countYearNow = calendarNow.get(Calendar.YEAR)
            val sumDaysNow = countDayNow + 30 * countMonthNow + 30 * 12 * countYearNow

            return sumDaysDate - sumDaysNow
        }

        fun getStringParseCountDaysToDay(date: String): String{
            val countDay = countBeetwenDateDateNow(date)
            val pref = mapOf(-2 to "Позавчера", -1 to "Вчера", 0 to "Сегодня", 1 to "Завтра", 2 to "Послезавтра")
            if (countDay in pref)
                return pref[countDay]!!
            val before = if (countDay > 0) "Через" else "Прошло "
            val after = if (countDay.toString()[countDay.toString().length - 1] == '1' && "11" !in countDay.toString()) "день" else
                    if (countDay.toString()[countDay.toString().length - 1].toString().toInt() in 2..4) "дня" else "дней"
            return "${before} ${kotlin.math.abs(countDay)} ${after}"
        }

        fun getDialogCheckFields(context: Context, mapFields: Map<String, String>): PlatformDialog? {
            val error: MutableList<MessageCodeDialog> = arrayListOf()
            for (i in mapFields.keys){
                val field = i.lowercase()
                val value = mapFields[field]!!

                if (value.isEmpty()) {
                    if (MessageCodeDialog.EMPTY_FIELD !in error) {
                        error.add(MessageCodeDialog.EMPTY_FIELD)
                    }else{
                        error.remove(MessageCodeDialog.EMPTY_FIELD)
                        error.add(MessageCodeDialog.EMPTY_FIELDS)
                    }
                }else {
                    if (field == "email" && !validateEmail(value))
                        error.add(MessageCodeDialog.EMAIL_NOT_CORRECT)
                    if (field == "password" && !validatePassword(value))
                        error.add(MessageCodeDialog.PASSWORD_NOT_CORRECT)

                }
            }
            if (error.size != 0) {
                if (error.size == 1)
                    return CreatorDialogs.getMessageDialog(context, codeDialog = error[0])
                return CreatorDialogs.getMessagesDialog(context, codesDialog = error)
            }
            return null
        }
    }
}
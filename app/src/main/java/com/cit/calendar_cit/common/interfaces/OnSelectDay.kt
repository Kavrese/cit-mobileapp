package com.cit.calendar_cit.common.interfaces

import com.cit.calendar_cit.common.models.ModelDay
import com.google.android.material.tabs.TabLayout

interface OnSelectDay {
    fun selectTabDay(modelDay: ModelDay, tab: TabLayout.Tab)
    fun unselectTabDay(modelDay: ModelDay, tab: TabLayout.Tab)
}
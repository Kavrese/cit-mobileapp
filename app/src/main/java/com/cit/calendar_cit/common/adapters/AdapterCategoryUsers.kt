package com.cit.calendar_cit.common.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.models.ModelCategoryUsersAvatars
import kotlinx.android.synthetic.main.item_category_user.view.*

class AdapterCategoryUsers(private val categoryUsers: List<ModelCategoryUsersAvatars>): RecyclerView.Adapter<AdapterCategoryUsers.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_category_user, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.title_category.text = categoryUsers[position].title
        holder.itemView.rec_avatars_category.apply {
            adapter = AdapterAvatars(categoryUsers[position].listAvatars, TypeAvatar.BIG)
            layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }
    }

    override fun getItemCount(): Int = categoryUsers.size
}
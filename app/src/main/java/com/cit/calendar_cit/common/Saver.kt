package com.cit.calendar_cit.common

import com.cit.calendar_cit.common.models.ModelDay
import com.cit.calendar_cit.common.models.ModelUser

object Saver {
    var token: String? = null
    lateinit var modelUser: ModelUser
    lateinit var modelDay: ModelDay
}
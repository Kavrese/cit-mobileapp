package com.cit.calendar_cit.common

import android.app.DatePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.interfaces.OnSelectDay
import com.cit.calendar_cit.common.models.ModelDay
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.view_tab_choose_date.view.*
import kotlinx.android.synthetic.main.view_tab_day.view.*
import kotlinx.android.synthetic.main.view_tab_day.view.card_tab
import java.util.*

class TabLayoutDays(val context: Context,
                    val tabLayout: TabLayout,
                    val days: List<ModelDay>,
                    val onSelectDay: OnSelectDay?=null,
                    val onChooseDateDay: OnSelectDay?=null
                                 ) {

    var prewSelectTabPosition: Int = 1
    var userAlreadySelectModelDay = false
    lateinit var modelDayDatePicker: ModelDay
    init {
        initTabsDays()
    }

    private fun initTabsDays(){
        setDaysToTabs(days)
        tabLayout.addTab(createChooseDateTab(), 0)
        selectTabWithVisual(1)
        addOnTabSelected()
    }

    private fun selectTabWithVisual(position: Int){
        tabLayout.getTabAt(position)!!.select()
        changeTabVisual(tabLayout.getTabAt(position)!!, true)
        prewSelectTabPosition = position
    }

    private fun setDaysToTabs(days: List<ModelDay>){
        for (day in days){
            val tab = tabLayout.newTab()
            tab.customView = getViewTab()
            tab.view.setPadding(0, 0, 0, 0)
            tab.customView!!.apply{
                this.week_day.text = day.dayOfWeek
                this.day.text = day.day.toString()
            }
            tabLayout.addTab(tab)
        }
    }

    private fun getViewTab(): View {
        return LayoutInflater.from(context).inflate(R.layout.view_tab_day, null)
    }

    private fun tapToChooseTab(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        DatePickerDialog(context, { datePicker, year, month, day ->
            val select_data = Calendar.getInstance()
            select_data.set(Calendar.YEAR, year)
            select_data.set(Calendar.MONTH, month)
            select_data.set(Calendar.DAY_OF_MONTH, day)
            val modelDay = Codes.modelDayFromDate(select_data.time)
            selectWithVisualChooseDate(modelDay)
        }, year, month, day).show()
    }

    private fun selectWithVisualChooseDate(modelDay: ModelDay){
        userAlreadySelectModelDay = true
        modelDayDatePicker = modelDay
        val tab = tabLayout.getTabAt(0)!!
        tab.view.title_choose_date.text = modelDay.date
        tab.select()
        changeTabVisual(tab, true)
        onChooseDateDay?.selectTabDay(modelDay, tab)
        userAlreadySelectModelDay = false
    }

    private fun addOnTabSelected(){
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val positionTab = tab!!.position
                if (positionTab != 0) {
                    //Не выбор даты
                    changeTabVisual(tab, true)
                    prewSelectTabPosition = positionTab
                    onSelectDay!!.selectTabDay(days[positionTab - 1], tab)
                }else{
                    //Выбор даты
                    if (userAlreadySelectModelDay) {   //Если пользователь уже выбрал дату
                        onChooseDateDay!!.selectTabDay(modelDayDatePicker, tab)
                    }else {
                        // Пользователю нужно быбрать дату
                        tapToChooseTab()
                        tabLayout.selectTab(tabLayout.getTabAt(prewSelectTabPosition))
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabVisual(tab!!, false)
                if (tab.position == 0){
                    userAlreadySelectModelDay = false
                    tab.customView!!.title_choose_date.text = "Выбрать дату"
                    if (::modelDayDatePicker.isInitialized)
                        onChooseDateDay?.unselectTabDay(modelDayDatePicker, tab)
                }else{
                    onSelectDay?.unselectTabDay(days[tab.position - 1], tab)
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                if (tab!!.position == 0 && !userAlreadySelectModelDay){
                    tapToChooseTab()
                }
            }
        })
    }

    private fun createChooseDateTab(): TabLayout.Tab {
        val tab = tabLayout.newTab()
        tab.customView = LayoutInflater.from(context).inflate(R.layout.view_tab_choose_date, null)
        return tab
    }

    private fun changeTabVisual(tab: TabLayout.Tab, isSelect: Boolean){
        tab.view.card_tab.setCardBackgroundColor(ContextCompat.getColor(context, if (isSelect) R.color.colorAccent else R.color.colorBackground))
    }
}
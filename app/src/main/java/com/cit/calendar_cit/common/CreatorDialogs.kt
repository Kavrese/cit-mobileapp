package com.cit.calendar_cit.common

import android.content.Context
import android.view.View
import com.cit.calendar_cit.Dialogs.PlatformDialog
import com.cit.calendar_cit.common.models.ModelTemplate
import com.cit.calendar_cit.common.models.ModelUser

class CreatorDialogs {
    companion object {
        fun getMessageDialog(context: Context, codeDialog: MessageCodeDialog, onClickButton1: View.OnClickListener?=null, onClickButton2: View.OnClickListener?=null, canDismiss:Boolean=true, t:Throwable?=null): PlatformDialog {
            return FactoryDialog.getDialog(context, messageCode=codeDialog, typeDialog=TypeDialog.MESSAGE_DIALOG, onClick1=onClickButton1, onClick2=onClickButton2, canDismissAfterClick=canDismiss, throwable=t)
        }

        fun getMessagesDialog(context: Context, codesDialog: List<MessageCodeDialog>, onClickButton1: View.OnClickListener?=null, onClickButton2: View.OnClickListener?=null, canDismiss:Boolean=true): PlatformDialog {
            return FactoryDialog.getDialog(context, messageCodes=codesDialog, typeDialog=TypeDialog.MESSAGES_DIALOG, onClick1=onClickButton1, onClick2=onClickButton2, canDismissAfterClick=canDismiss)
        }

        fun getChooseDialog(context: Context, data: Map<String, View.OnClickListener>, canDismiss:Boolean=true): PlatformDialog{
            return FactoryDialog.getDialog(context, mapButtons=data, typeDialog=TypeDialog.CHOOSE_DIALOG, canDismissAfterClick=canDismiss)
        }

        fun getListUserDialog(context: Context, dataList: List<ModelUser>, onClickElementList: View.OnClickListener?=null, canDismiss:Boolean=true): PlatformDialog{
            return FactoryDialog.getDialog(context, dataList=dataList, onClickElementList=onClickElementList, typeDialog=TypeDialog.DIALOG_LIST_USERS, messageCode=MessageCodeDialog.LIST_DIALOG_USERS, canDismissAfterClick=canDismiss)
        }

        fun getListTemplateDialog(context: Context, dataList: List<ModelTemplate>, onClickElementList: View.OnClickListener?=null, canDismiss:Boolean=true): PlatformDialog{
            return FactoryDialog.getDialog(context, dataList=dataList, onClickElementList=onClickElementList, typeDialog=TypeDialog.DIALOG_LIST_TEMPLATES, messageCode=MessageCodeDialog.LIST_DIALOG_TEMPLATES, canDismissAfterClick=canDismiss)
        }
    }
}
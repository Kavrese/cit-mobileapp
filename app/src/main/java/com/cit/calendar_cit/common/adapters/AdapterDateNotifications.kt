package com.cit.calendar_cit.common.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.interfaces.OnClickEvent
import com.cit.calendar_cit.common.models.ModelNotification
import kotlinx.android.synthetic.main.item_date_notification.view.*
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.item_time_event.view.*

class AdapterDateNotifications(notifications: List<ModelNotification>, private val onClickEvent: OnClickEvent): RecyclerView.Adapter<AdapterDateNotifications.ViewHolder>() {
    private lateinit var dateNotifications: List<ModelDateNotification>
    init {
        setNewList(notifications)
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    class AdapterTimeNotification(notifications: List<ModelNotification>, private val onClickEvent: OnClickEvent): RecyclerView.Adapter<ViewHolder>(){
        val times = notifications.map { it.time }.toSet().toList()
        private val timeNotifications = times.map { time -> ModelTimeNotification(time, notifications.filter { it.time == time })}

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_time_event, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.time_events.text = timeNotifications[position].time
            holder.itemView.rec_events.apply {
                layoutManager = LinearLayoutManager(holder.itemView.context)
                adapter = AdapterNotification(timeNotifications[position].notifications, onClickEvent)
            }
            holder.itemView.up_part.setImageResource(if (position == 0) R.drawable.up else R.drawable.dot)
            holder.itemView.down_part.visibility = if (position == timeNotifications.lastIndex) View.VISIBLE else View.GONE
        }

        override fun getItemCount(): Int = timeNotifications.size

    }

    class AdapterNotification(private val notifications: List<ModelNotification>, private val onClickEvent: OnClickEvent): RecyclerView.Adapter<ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false))
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.title_event.text = notifications[position].event!!.title
            holder.itemView.duration_event.text = "${notifications[position].event!!.durationHour} ${
                Codes.getStrAfterHour(
                    notifications[position].event!!.durationHour
                )
            }"
            holder.itemView.rec_avatars.apply {
                layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = AdapterAvatars(StaticData.listAvatarsBoss, TypeAvatar.SMALL)
            }
            holder.itemView.setOnClickListener {
                onClickEvent.onClick(notifications[position].event!!)
            }
        }

        override fun getItemCount(): Int = notifications.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_date_notification, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.date_notifications.text = dateNotifications[position].date
        holder.itemView.rec_notifications.apply {
            layoutManager = LinearLayoutManager(holder.itemView.context)
            adapter = AdapterTimeNotification(dateNotifications[position].notifications, onClickEvent)
        }
    }

    override fun getItemCount(): Int = dateNotifications.size

    fun setNewList(newData: List<ModelNotification>){
        val dates = newData.map { it.date }.toSet().toList()
        dateNotifications = dates.map { date -> ModelDateNotification(date, newData.filter { it.date == date }) }
        notifyDataSetChanged()
    }
}

data class ModelDateNotification(
    val date: String,
    val notifications: List<ModelNotification>
)

data class ModelTimeNotification(
    val time: String,
    val notifications: List<ModelNotification>
)
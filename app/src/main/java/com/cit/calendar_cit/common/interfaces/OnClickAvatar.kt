package com.cit.calendar_cit.common.interfaces

import com.cit.calendar_cit.common.models.ModelAvatar

interface OnClickAvatar {
    fun onCLickAvatar(modelAvatar: ModelAvatar)
}
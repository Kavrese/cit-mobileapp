package com.cit.calendar_cit.common.models

data class ModelDay(
    val date: String,   //Формат dd.MM.yy
    val day: Int,
    val dayOfWeek: String,
)

package com.cit.calendar_cit.common.models

data class ModelDayData(
    val day: ModelDay,
    val events: List<ModelEvent>
)

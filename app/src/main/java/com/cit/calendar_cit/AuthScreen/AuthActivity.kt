package com.cit.calendar_cit.AuthScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.cit.calendar_cit.Dialogs.PlatformDialog
import com.cit.calendar_cit.MainScreen.MainActivity
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.Saver
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.interfaces.OnAuth
import com.cit.calendar_cit.common.models.ModelUser
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        Handler().postDelayed({
            if (checkIsFirstEnter())
                toAuth()
            else
                toMain()
        }, 1500)
    }

    private fun toAuth(){
        motion.transitionToEnd {
            prepareAuthLayout()
        }
    }

    private fun prepareAuthLayout(){
        card_enter.setOnClickListener {
            toMain() // Пока нет API
            val login = login.text.toString()
            val password = password.text.toString()
            val dialog: PlatformDialog? = Codes.getDialogCheckFields(this, mapOf(
                "login" to login,
                "password" to password
            ))
            if (dialog != null)
                dialog.show()
            else
                Codes.auth(this, login, password, object: OnAuth{
                    override fun onGood(modelUser: ModelUser) {
                        Saver.modelUser = modelUser
                        Saver.token = modelUser.token
                        setNoFirstEnter()
                    }
                })
        }
    }

    private fun toMain(){
        Saver.modelUser = StaticData.modelUser  //Пока нет API
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun checkIsFirstEnter(): Boolean{
        val sh = getSharedPreferences("0", 0)
        return sh.getBoolean("isAuth", true)
    }

    private fun setNoFirstEnter() {
        val sh = getSharedPreferences("0", 0)
        sh.edit()
            .putBoolean("isAuth", false)
            .apply()
    }
}
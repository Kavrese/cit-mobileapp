package com.cit.calendar_cit.Dialogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.TypeDialog
import com.cit.calendar_cit.common.models.ModelTemplate
import com.cit.calendar_cit.common.models.ModelUser
import com.cit.calendar_cit.common.models.TypeAccess
import kotlinx.android.synthetic.main.item_template.view.*
import kotlinx.android.synthetic.main.item_user.view.*
import kotlinx.android.synthetic.main.layout_choose_dialog.*
import kotlinx.android.synthetic.main.layout_list_dialog.*
import kotlinx.android.synthetic.main.layout_list_dialog.view.*
import kotlinx.android.synthetic.main.layout_message_dialog.*

class ListDialog
    (context: Context, private val title: String, typeDialog: TypeDialog, dismissAfterClickButton:Boolean = true): PlatformDialog(context, typeDialog, canDismiss = dismissAfterClickButton) {
    init {setCancelable(true)}

    var adapterListDialog: AdapterListDialog? = null


    class Builder(private val context: Context) {
        private var data: List<Any> = arrayListOf()
        private var title: String = ""
        private var typeDialog: TypeDialog=TypeDialog.DIALOG_LIST_USERS
        private var onClickElement: View.OnClickListener? = null
        private var dismissAfterClickButton: Boolean =true

        fun setTitle(title: String) = apply{this.title = title}
        fun setDataForAdapter(data: List<Any>, typeDialog: TypeDialog) = apply {
            this.typeDialog = typeDialog
            this.data = data
        }
        fun setDismissAfterClickButton(canDismiss: Boolean) = apply {this.dismissAfterClickButton = canDismiss}
        fun setOnClickElement(onClickElement: View.OnClickListener?) = apply { this.onClickElement = onClickElement }

        fun create(): PlatformDialog {
            val dialog = ListDialog(context, title, typeDialog)
            dialog.adapterListDialog = AdapterListDialog(data, onClickElement, dialog)
            dialog.create()
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_list_dialog)
        window!!.decorView.setBackgroundResource(android.R.color.transparent)

        title_dialog_list.text = title
        rec_dialog_list.apply {
            adapter = adapterListDialog
            layoutManager = LinearLayoutManager(context)
        }
    }

    class AdapterListDialog(private val data: List<Any>, private val onClickElement: View.OnClickListener?,
                            private val dialog: PlatformDialog): RecyclerView.Adapter<AdapterListDialog.ViewHolder>() {

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(
                when(dialog.typeDialog) {
                    TypeDialog.DIALOG_LIST_TEMPLATES -> R.layout.item_template
                    TypeDialog.DIALOG_LIST_USERS -> R.layout.item_user
                    else -> R.layout.item_template
                }, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (dialog.typeDialog == TypeDialog.DIALOG_LIST_TEMPLATES){
                val template = data[position] as ModelTemplate
                holder.itemView.count_users.text = template.users.size.toString()
                holder.itemView.title_template.text = template.title
                holder.itemView.type_template.text = template.access.title
            }
            if (dialog.typeDialog == TypeDialog.DIALOG_LIST_USERS){
                val user = data[position] as ModelUser
                holder.itemView.fio.text = "${user.lastname} ${user.firstname} ${user.lastname}"
                holder.itemView.role.text = user.type.title
            }
            holder.itemView.setOnClickListener {
                dialog.dismissIfCan()
                onClickElement?.onClick(it)
            }
        }

        override fun getItemCount(): Int = data.size
    }
}
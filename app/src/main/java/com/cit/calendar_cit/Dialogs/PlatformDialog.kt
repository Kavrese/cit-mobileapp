package com.cit.calendar_cit.Dialogs

import android.app.Dialog
import android.content.Context
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.TypeDialog

open class PlatformDialog(context: Context,
                          val typeDialog: TypeDialog,
                          themeRes: Int=R.style.Theme_AppCompat_Light_Dialog_Alert,
                          var canDismiss:Boolean = true): Dialog(context, themeRes) {
    fun dismissIfCan() {
        if (canDismiss)
            dismiss()
    }
}
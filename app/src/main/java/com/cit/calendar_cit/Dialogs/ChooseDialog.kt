package com.cit.calendar_cit.Dialogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.TypeDialog
import kotlinx.android.synthetic.main.item_button.view.*
import kotlinx.android.synthetic.main.layout_choose_dialog.*
import kotlinx.android.synthetic.main.layout_message_dialog.*

class ChooseDialog(context: Context, var dismissAfterClickButton:Boolean=true): PlatformDialog(context, TypeDialog.CHOOSE_DIALOG, canDismiss=dismissAfterClickButton) {
    init {setCancelable(true)}

    var adapterChooseDialog: AdapterButtonChooseDialog? = null


    class Builder(context: Context) {
        private val dialog = ChooseDialog(context)
        private var data: Map<String, View.OnClickListener> = mapOf()

        fun setDismissAfterClick(canDismiss: Boolean) = apply {dialog.dismissAfterClickButton=canDismiss}
        fun setDataForAdapter(data: Map<String, View.OnClickListener>) = apply { this.data = data }

        fun create(): PlatformDialog {
            dialog.adapterChooseDialog = AdapterButtonChooseDialog(data, dialog)
            dialog.create()
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_choose_dialog)
        window!!.decorView.setBackgroundResource(android.R.color.transparent)

        rec_button.apply {
            adapter = this@ChooseDialog.adapterChooseDialog
            layoutManager = LinearLayoutManager(context)
        }

        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(context, R.drawable.line)!!)
        rec_button.addItemDecoration(dividerItemDecoration)
    }

    class AdapterButtonChooseDialog(private val buttons: Map<String, View.OnClickListener>, private val dialog: PlatformDialog): RecyclerView.Adapter<AdapterButtonChooseDialog.ViewHolder>() {

        private val listKeys = buttons.keys.toList()
        private val listOnClickListener = listKeys.map {buttons[it]}

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_button, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.setOnClickListener {
                listOnClickListener[position]!!.onClick(holder.itemView)
                dialog.dismissIfCan()
            }
            holder.itemView.title_button.text = listKeys[position]
        }

        override fun getItemCount(): Int = buttons.size
    }
}
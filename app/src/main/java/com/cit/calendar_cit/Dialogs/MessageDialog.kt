package com.cit.calendar_cit.Dialogs

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.TypeDialog
import kotlinx.android.synthetic.main.layout_message_dialog.*

class MessageDialog(context: Context, var dismissAfterClickButton: Boolean=true): PlatformDialog(context, TypeDialog.MESSAGE_DIALOG, canDismiss = dismissAfterClickButton) {
    init {setCancelable(true)}

    var title = ""
    var text1 = "Да"
    var text2 = "Нет"
    var onClickButtonText1: View.OnClickListener? = null
    var onClickButtonText2: View.OnClickListener? = null


    class Builder(context: Context) {
        private val dialog = MessageDialog(context)

        fun setTitle(title: String) = apply { dialog.title = title}
        fun setMessage(message: String) = apply {dialog.title = "${dialog.title}. $message"}
        fun setOnClickButton1(onClickListener: View.OnClickListener?) = apply { dialog.onClickButtonText1 = onClickListener}
        fun setOnClickButton2(onClickListener: View.OnClickListener?) = apply { dialog.onClickButtonText2 = onClickListener}
        fun setTextButton2(text2: String?) = apply { if (text2 != null) dialog.text2 = text2 }
        fun setTextButton1(text1: String?) = apply { if (text1 != null) dialog.text1 = text1 }
        fun setDismissAfterClickButton(canDismiss: Boolean) = apply {dialog.dismissAfterClickButton = canDismiss}

        fun create(): PlatformDialog {
            dialog.create()
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_message_dialog)
        window!!.decorView.setBackgroundResource(android.R.color.transparent)

        title_dialog_message.text = title
        button_1_dialog_message.text = text1
        button_2_dialog_message.text = text2

        if (onClickButtonText1 != null)
            button_1_dialog_message.setOnClickListener {
                onClickButtonText1!!.onClick(it)
            }
        else
            button_1_dialog_message.visibility = View.GONE

        if (onClickButtonText2 != null)
            button_2_dialog_message.setOnClickListener {
                onClickButtonText2!!.onClick(it)
            }
        else
            button_2_dialog_message.visibility = View.GONE
    }
}
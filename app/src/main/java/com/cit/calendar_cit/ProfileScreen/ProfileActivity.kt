package com.cit.calendar_cit.ProfileScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.ToNextScreen
import com.cit.calendar_cit.common.models.ModelUser
import com.cit.calendar_cit.common.models.TypeUser
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    private val modelAvatar = ToNextScreen.avatar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val modelUser = getUserModel()
        lastname.text = modelUser.lastname
        secondname.text = modelUser.secondname
        firstname.text = modelUser.firstname
        Glide.with(this)
            .load(modelUser.avatarURL)
            .into(avatar_profile)
        type_profile.text = modelUser.type.title

        back_profile.setOnClickListener {
            finish()
        }
    }

    private fun getUserModel(): ModelUser{
        // Пока нет API
        return ModelUser(0, modelAvatar.firstname, modelAvatar.secondname, modelAvatar.lastname, modelAvatar.avatar, 0, null)
    }
}
package com.cit.calendar_cit.LessonScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.adapters.AdapterCategoryUsers
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.ToNextScreen
import kotlinx.android.synthetic.main.activity_lesson.*

class LessonActivity : AppCompatActivity() {

    val modelEvent = ToNextScreen.event
    companion object {
        val VIEW = 0
        val EDIT = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson)

        back.setOnClickListener{
            finish()
        }

        title_event_lesson.text = modelEvent.title
        date_event_lesson.text = "${modelEvent.date} ${if (Codes.countBeetwenDateDateNow(modelEvent.date) < 0) "- Прошло" else ""}"
        time_event_lesson.text = modelEvent.time
        duration_event_lesson.text = "${modelEvent.durationHour} ${Codes.getStrAfterHour(modelEvent.durationHour)}"

        rec_users.apply {
            adapter = AdapterCategoryUsers(StaticData.listCategoryAvatars)
            layoutManager = LinearLayoutManager(this@LessonActivity)
        }
        if (modelEvent.href != null)
            url_event_lesson.setOnClickListener {
                Codes.openUrlInBrowser(this, modelEvent.href)
            }
        else
            url_event_lesson.visibility = View.INVISIBLE
    }
}
package com.cit.calendar_cit.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.adapters.AdapterDateNotifications
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.interfaces.OnClickEvent
import com.cit.calendar_cit.common.models.ModelEvent
import com.cit.calendar_cit.common.models.ModelNotification
import kotlinx.android.synthetic.main.fragment_calendar_day.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_notification.swipe_refresh_layout

class FragmentNotification: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }
    var notifications: MutableList<ModelNotification> = arrayListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rec_date_notifications.apply {
            adapter = AdapterDateNotifications(notifications, object: OnClickEvent{
                override fun onClick(event: ModelEvent) {

                }
            })
            layoutManager = LinearLayoutManager(requireContext())
        }
        swipe_refresh_layout.setOnRefreshListener {
            updateData()
        }
        rec_date_notifications.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                //Для SwiperRefreshLayout
                val topRowVerticalPosition = if (rec_times == null || rec_times.childCount == 0) 0 else rec_times.getChildAt(0).top
                swipe_refresh_layout.isEnabled = topRowVerticalPosition >= 0
            }
        })
        updateData()
    }

    private fun updateData(){
        val newData = getData()
        notifications.clear()
        notifications.addAll(newData)
        if (newData.isEmpty()){
            no_notifications.visibility = View.VISIBLE
            rec_date_notifications.visibility = View.GONE
        }else{
            no_notifications.visibility = View.INVISIBLE
            rec_date_notifications.visibility = View.VISIBLE
            (rec_date_notifications.adapter as AdapterDateNotifications).setNewList(newData)
        }
        swipe_refresh_layout.isRefreshing = false
    }

    private fun getData(): List<ModelNotification>{
        // Пока нет API
        return StaticData.notifications
    }
}
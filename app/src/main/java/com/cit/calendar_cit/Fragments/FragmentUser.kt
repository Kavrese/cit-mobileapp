package com.cit.calendar_cit.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.cit.calendar_cit.AuthScreen.AuthActivity
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.Saver
import kotlinx.android.synthetic.main.fragment_user.*

class FragmentUser: Fragment() {

    val modelUser = Saver.modelUser

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        lastname.text = modelUser.lastname
        secondname.text = modelUser.secondname
        firstname.text = modelUser.firstname
        type_profile.text = modelUser.type.title

        exit_profile.setOnClickListener {
            requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java))
            requireActivity().finish()
        }

        avatar_profile.setOnLongClickListener {
            //Для изменения аватарки
            return@setOnLongClickListener true
        }

        if (modelUser.avatarURL != null)
            Glide.with(requireContext())
                .load(modelUser.avatarURL)
                .into(avatar_profile)
    }
}
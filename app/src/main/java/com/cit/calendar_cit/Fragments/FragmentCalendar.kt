package com.cit.calendar_cit.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.adapters.AdapterFragmentCalendarDay
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.Saver
import com.cit.calendar_cit.common.TabLayoutDays
import com.cit.calendar_cit.common.interfaces.OnSelectDay
import com.cit.calendar_cit.common.models.ModelDay
import com.cit.calendar_cit.common.models.TypeUser
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_calendar.*

class FragmentCalendar: Fragment() {
    lateinit var nowModelDay: ModelDay
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val days = Codes.getSevenDays()
        pager_days.adapter = AdapterFragmentCalendarDay(requireActivity(), days)

        TabLayoutDays(requireContext(), tab_days, days,
            object: OnSelectDay{
                override fun selectTabDay(modelDay: ModelDay, tab: TabLayout.Tab) {
                    nowModelDay = modelDay
                    pager_days.setCurrentItem(tab.position - 1, true)
                }

                override fun unselectTabDay(modelDay: ModelDay, tab: TabLayout.Tab) {}
            },
            object: OnSelectDay{
                override fun selectTabDay(modelDay: ModelDay, tab: TabLayout.Tab) {
                    nowModelDay = modelDay
                    fragment_choose_date.visibility = View.VISIBLE
                    parentFragmentManager.beginTransaction()
                        .replace(R.id.fragment_choose_date, FragmentCalendarDay(modelDay))
                        .commit()
                }

                override fun unselectTabDay(modelDay: ModelDay, tab: TabLayout.Tab) {
                    fragment_choose_date.visibility = View.GONE
                }
            }
        )

        pager_days.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tab_days.selectTab(tab_days.getTabAt(position + 1))
            }
        })

        if (Saver.modelUser.type == TypeUser.TYPE_ADMIN || Saver.modelUser.type == TypeUser.TYPE_ASSISTANT) {
            add_event.visibility = View.VISIBLE
            add_event.setOnClickListener {
                Saver.modelDay = nowModelDay

            }
        }else{
            add_event.visibility = View.GONE
        }
    }
}
package com.cit.calendar_cit.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cit.calendar_cit.LessonScreen.LessonActivity
import com.cit.calendar_cit.R
import com.cit.calendar_cit.common.adapters.AdapterTimeEvents
import com.cit.calendar_cit.common.Codes
import com.cit.calendar_cit.common.StaticData
import com.cit.calendar_cit.common.ToNextScreen
import com.cit.calendar_cit.common.interfaces.OnClickEvent
import com.cit.calendar_cit.common.models.ModelDay
import com.cit.calendar_cit.common.models.ModelDayData
import com.cit.calendar_cit.common.models.ModelEvent
import kotlinx.android.synthetic.main.fragment_calendar_day.*

class FragmentCalendarDay(private val modelDay: ModelDay?): Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_calendar_day, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        count_days.text = Codes.getStringParseCountDaysToDay(modelDay!!.date)

        rec_times.apply {
            adapter = AdapterTimeEvents(arrayListOf(), object: OnClickEvent{
                override fun onClick(event: ModelEvent) {
                    ToNextScreen.event = event
                    startActivity(Intent(requireContext(), LessonActivity::class.java))
                }
            })
            layoutManager = LinearLayoutManager(requireContext())
        }
        swipe_refresh_layout.setOnRefreshListener {
            updateData()
        }
        rec_times.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                //Для SwiperRefreshLayout
                val topRowVerticalPosition = if (rec_times == null || rec_times.childCount == 0) 0 else rec_times.getChildAt(0).top
                swipe_refresh_layout.isEnabled = topRowVerticalPosition >= 0
            }
        })
        updateData()
    }

    private fun updateData(){
        val data = getDataDay()
        if (data == null || data.events.isEmpty())
            no_events.visibility = View.VISIBLE
        else
            (rec_times.adapter as AdapterTimeEvents).newListEvents(data.events)
        swipe_refresh_layout.isRefreshing = false
    }

    private fun getDataDay(): ModelDayData?{
        // Когда будет готово API заменить на неё, а пока тестовые данные
        val list_ = StaticData.listDataDays.filter { modelDayData: ModelDayData -> modelDayData.day.date == modelDay!!.date}
        return if (list_.isNotEmpty()) list_[0] else null
    }
}